export const APP_CONFIG = {
  production: true,
  environment: 'PROD',
  apiUrl: 'https://demo-qz0h.onrender.com/api/',
  apiBack: 'http://localhost:1224',
};
