export const APP_CONFIG = {
  production: false,
  environment: 'LOCAL',
  apiUrl: 'https://demo-qz0h.onrender.com/api/',
  apiBack: 'http://localhost:1224',
};
