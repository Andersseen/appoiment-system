import { User } from '../calendar/day-view/day-view-scheduler-calendar.utils';

export const Users: User[] = [
  {
    id: 0,
    name: '',
  },
  {
    id: 1,
    name: '',
  },
  {
    id: 2,
    name: '',
  },
  {
    id: 3,
    name: 'Rayos Uva',
  },
  {
    id: 4,
    name: 'Notas',
  },
];
