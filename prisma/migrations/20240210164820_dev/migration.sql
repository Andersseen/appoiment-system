-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Event" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "title" TEXT NOT NULL,
    "start" DATETIME NOT NULL,
    "end" DATETIME,
    "colorId" INTEGER NOT NULL,
    "sectionId" INTEGER NOT NULL,
    "clientId" INTEGER NOT NULL,
    "notes" TEXT,
    "draggable" BOOLEAN NOT NULL,
    "beforeStart" BOOLEAN NOT NULL,
    "afterEnd" BOOLEAN NOT NULL,
    CONSTRAINT "Event_colorId_fkey" FOREIGN KEY ("colorId") REFERENCES "Color" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Event_sectionId_fkey" FOREIGN KEY ("sectionId") REFERENCES "Section" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Event_clientId_fkey" FOREIGN KEY ("clientId") REFERENCES "Client" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Event" ("afterEnd", "beforeStart", "clientId", "colorId", "draggable", "end", "id", "notes", "sectionId", "start", "title") SELECT "afterEnd", "beforeStart", "clientId", "colorId", "draggable", "end", "id", "notes", "sectionId", "start", "title" FROM "Event";
DROP TABLE "Event";
ALTER TABLE "new_Event" RENAME TO "Event";
CREATE TABLE "new_Client" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" TEXT NOT NULL,
    "lastName" TEXT NOT NULL,
    "email" TEXT,
    "phone" TEXT,
    "notes" TEXT
);
INSERT INTO "new_Client" ("email", "id", "lastName", "name", "notes", "phone") SELECT "email", "id", "lastName", "name", "notes", "phone" FROM "Client";
DROP TABLE "Client";
ALTER TABLE "new_Client" RENAME TO "Client";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
