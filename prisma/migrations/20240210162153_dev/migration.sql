/*
  Warnings:

  - Added the required column `clientId` to the `Event` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Event" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "title" TEXT NOT NULL,
    "start" DATETIME NOT NULL,
    "end" DATETIME NOT NULL,
    "colorId" INTEGER NOT NULL,
    "sectionId" INTEGER NOT NULL,
    "clientId" INTEGER NOT NULL,
    "notes" TEXT NOT NULL,
    "draggable" BOOLEAN NOT NULL,
    "beforeStart" BOOLEAN NOT NULL,
    "afterEnd" BOOLEAN NOT NULL,
    CONSTRAINT "Event_colorId_fkey" FOREIGN KEY ("colorId") REFERENCES "Color" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Event_sectionId_fkey" FOREIGN KEY ("sectionId") REFERENCES "Section" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Event_clientId_fkey" FOREIGN KEY ("clientId") REFERENCES "Client" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Event" ("afterEnd", "beforeStart", "colorId", "draggable", "end", "id", "notes", "sectionId", "start", "title") SELECT "afterEnd", "beforeStart", "colorId", "draggable", "end", "id", "notes", "sectionId", "start", "title" FROM "Event";
DROP TABLE "Event";
ALTER TABLE "new_Event" RENAME TO "Event";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
