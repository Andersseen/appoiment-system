-- AlterTable
ALTER TABLE "Section" ADD COLUMN "order" INTEGER;

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Color" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" TEXT NOT NULL,
    "primary" TEXT NOT NULL,
    "secondary" TEXT NOT NULL,
    "secondaryText" TEXT
);
INSERT INTO "new_Color" ("id", "name", "primary", "secondary", "secondaryText") SELECT "id", "name", "primary", "secondary", "secondaryText" FROM "Color";
DROP TABLE "Color";
ALTER TABLE "new_Color" RENAME TO "Color";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
